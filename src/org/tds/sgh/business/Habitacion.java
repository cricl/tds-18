package org.tds.sgh.business;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Habitacion
{
	// --------------------------------------------------------------------------------------------
	
	private String nombre;
	
	protected long id;
	
	private TipoHabitacion tipoHabitacion;
	
	// --------------------------------------------------------------------------------------------
	
	public Habitacion(TipoHabitacion tipoHabitacion, String nombre)
	{
		this.nombre = nombre;
		
		this.tipoHabitacion = tipoHabitacion;
	}
	
	// --------------------------------------------------------------------------------------------
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId()
	{
		return this.id;
	}
	
	protected void setId(long id)
	{
		this.id = id;
	}
	
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	
	@OneToOne(cascade=CascadeType.ALL)
	public TipoHabitacion getTipoHabitacion()
	{
		return this.tipoHabitacion;
	}
	
	public void setTipoHabitacion(TipoHabitacion tipoHabitacion)
	{
		this.tipoHabitacion = tipoHabitacion;
	}
	
	//----------------------------------------------------------------------
	
	public boolean esDelTipoHabitacion(TipoHabitacion tipohabitacion){
		if (getTipoHabitacion().getNombre() == tipohabitacion.getNombre()){
			return true;
		}
		else{
			return false;
		}		
	}	
}
