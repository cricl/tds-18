package org.tds.sgh.business;
import java.util.Arrays;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;

import org.tds.sgh.infrastructure.ICalendario;
import org.tds.sgh.infrastructure.Infrastructure;
import javax.persistence.*;

@Entity
public class Reserva {

	protected long id;
	private long codigo;
	private boolean modificablePorHuesped;
	private GregorianCalendar fechaInicio;
	private GregorianCalendar fechaFin;
	private EstadoReserva estadoReserva;
	private Set<Huesped> huespedes;
	private TipoHabitacion tipohabitacion;
	private Habitacion habitacion;
	private Hotel hotel;
	private String rutCliente;
	
	
	public Reserva(String rut_cliente, Hotel hotel, TipoHabitacion tipohabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin, boolean modificablePorHuesped, EstadoReserva estadoReserva) {
		Random rand = new Random();
		long  codigo = rand.nextInt(50) + 1;
	
		this.codigo = codigo;
		
		this.hotel = hotel;
		
		this.tipohabitacion = tipohabitacion;
		
		this.modificablePorHuesped = modificablePorHuesped;
		
		this.fechaInicio = fechaInicio;
		
		this.fechaFin = fechaFin;
		
		this.estadoReserva = estadoReserva;
		
		this.huespedes = new HashSet<>();
		
		this.habitacion = null;
		
		this.rutCliente = rut_cliente;
	}
	
	
	//---------------------------getters y setters----------------------------
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId()
	{
		return this.id;
	}
	
	protected void setId(long id)
	{
		this.id = id;
	}
	
	public long getCodigo() {
		return this.codigo;
	}
	
	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}
	
	public boolean isModificablePorHuesped() {
		return this.modificablePorHuesped;
	}

	public void setModificablePorHuesped(boolean modificablePorHuesped) {
		this.modificablePorHuesped = modificablePorHuesped;
	}
	
	public GregorianCalendar getFechaInicio() {
		return this.fechaInicio;
	}
	
	public void setFechaInicio(GregorianCalendar fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public void setFechaFin(GregorianCalendar fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	
	public GregorianCalendar getFechaFin() {
		return fechaFin;
	}

	public EstadoReserva getEstadoReserva() 
	{
		return this.estadoReserva;
	}
	
	public void setEstadoReserva(EstadoReserva estadoReserva) 
	{
		this.estadoReserva = estadoReserva;
	}
	
	public void cambiarEstado(EstadoReserva estadoReserva) 
	{
		this.estadoReserva = estadoReserva;
	}
	
	@OneToMany(cascade=CascadeType.ALL)
	public Set<Huesped> getHuespedes() {
		return this.huespedes;
	}
	
	public void setHuespedes(Set<Huesped> huespedes) {
		this.huespedes = huespedes;
	}
	
	@ManyToOne(cascade=CascadeType.ALL)
	public TipoHabitacion getTipoHabitacion() {
		return this.tipohabitacion;
	}
	
	public void setTipoHabitacion(TipoHabitacion tipohabitacion) {
		this.tipohabitacion = tipohabitacion;
	}
	
	@OneToOne(cascade=CascadeType.ALL)
	public Habitacion getHabitacion() {
		return this.habitacion;
	}
	
	public void setHabitacion(Habitacion habitacion) {
		this.habitacion = habitacion;
	}
	
	@ManyToOne(cascade=CascadeType.ALL)
	public Hotel getHotel() {
		return this.hotel;
	}
	
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	
	
	public String getRutCliente() {
		return this.rutCliente;
	}
	
	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}
	

	public EstadoReserva getEstado() {
		return estadoReserva;
	}

	public void setEstado(EstadoReserva estadoReserva) {
		this.estadoReserva = estadoReserva;
	}
	//----------------------------------------------------------------------
	
	public boolean registrarHuesped(String nombre, String documento)
	{
		Huesped h = new Huesped(nombre, documento);
		this.huespedes.add(h);
		return true;
	}
	
	public boolean tieneConflictos(TipoHabitacion tipohabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin, EstadoReserva estadoReserva) 
	{	
		ICalendario cal = Infrastructure.getInstance().getCalendario();
		String[] estados =  {EstadoReserva.NoTomada.toString(),EstadoReserva.Cancelada.toString(), EstadoReserva.Finalizada.toString()};
		GregorianCalendar fecha = new GregorianCalendar();
		
		if(this.getTipoHabitacion().equals(tipohabitacion)){
			if(!Arrays.asList(estados).contains(this.estadoReserva.toString())) {
				fecha.setTime(fechaInicio.getTime());
				while(cal.esAnterior(fecha,fechaFin) || cal.esMismoDia(fecha, fechaFin)) {
					if((cal.esPosterior( fecha,this.fechaInicio) 
							|| cal.esMismoDia( fecha,this.fechaInicio))  
							&& 
							(cal.esAnterior( fecha,this.fechaFin)))
					{
						return true;
					}
					fecha.add(Calendar.DATE, 1);
				}	
			}
		}
		return false;
	}
	
	public Reserva registrarReserva(String rut_cliente, Hotel h, TipoHabitacion th, GregorianCalendar fechaInicio, GregorianCalendar fechaFin, boolean modificablePorUsuario)
	{		
		Reserva reserva = new Reserva(rut_cliente, h, th, fechaInicio, fechaFin, modificablePorUsuario, EstadoReserva.Pendiente);
		return reserva;
	}
	
	public Reserva modificarReserva(String rutCliente, Hotel h, TipoHabitacion th, GregorianCalendar fechaInicio, GregorianCalendar fechaFin, boolean modificablePorUsuario) {
		this.setHotel(h);
		this.setTipoHabitacion(th);
		this.setFechaInicio(fechaInicio);
		this.setFechaFin(fechaFin);
		this.setModificablePorHuesped(modificablePorUsuario);
		return this;
	}
	
	public void cancelarReserva() {
		this.setEstado(EstadoReserva.Cancelada);
	}
	
	public void asignarHabitacion(Habitacion habitacion) {
		this.habitacion = habitacion;
	}
	

}
