package org.tds.sgh.business;

import java.util.GregorianCalendar;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.persistence.*;

@Entity
public class Hotel
{
	// --------------------------------------------------------------------------------------------
	
	private Map<String, Habitacion> habitaciones;
	
	private Map<Long, Reserva> reservas;
	
	private String nombre;
	
	private String pais;
	
	protected long id;
	
	// --------------------------------------------------------------------------------------------
	
	public Hotel(String nombre, String pais)
	{
		this.habitaciones = new HashMap<String, Habitacion>();
		
		this.reservas = new HashMap<Long, Reserva>();
		
		this.nombre = nombre;
		
		this.pais = pais;
	}
	
	// --------------------------------------------------------------------------------------------
	
	public Habitacion agregarHabitacion(TipoHabitacion tipoHabitacion, String nombre) throws Exception
	{
		if (this.habitaciones.containsKey(nombre))
		{
			throw new Exception("El hotel ya tiene una habitación con el nombre indicado.");
		}
		
		Habitacion habitacion = new Habitacion(tipoHabitacion, nombre);
		
		this.habitaciones.put(habitacion.getNombre(), habitacion);
		
		return habitacion;
	}
	
	//-----------------------------------------------------------------

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId()
	{
		return this.id;
	}
	
	protected void setId(long id)
	{
		this.id = id;
	}
	
	
	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="id")
	public Map<String, Habitacion> getHabitacion(){
		
		return this.habitaciones;
	}
	
	public void setHabitacion(Map<String, Habitacion> habitaciones){
		
		this.habitaciones = habitaciones;
	}
	
	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="codigo")
	public Map<Long, Reserva> getReserva(){
		
		return this.reservas;
	}
	
	public void setReserva(Map<Long, Reserva> reservas){
		
		this.reservas = reservas;
	}
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	
	public String getPais()
	{
		return this.pais;
	}
	
	public void setPais(String pais)
	{
		this.pais = pais;
	}
	

	
	//---------------------------------------------------------------------
	
	public Set<Habitacion> listarHabitaciones()
	{
		return new HashSet<Habitacion>(this.habitaciones.values());
	}
	
	public int calcCapacidad(TipoHabitacion tipoHabitacion) {
		int cantHab=0;
		for(Habitacion anObject : habitaciones.values()){
			//do someting to anObject...
		    if(anObject.esDelTipoHabitacion(tipoHabitacion)) cantHab++;
		}
		return cantHab;
	}
	
	public int calcReservaConDif(Reserva reserva, TipoHabitacion tipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin) {
		int cantReser=0;
		for(Reserva anObject : reservas.values()){
			//do someting to anObject...
			if(!anObject.equals(reserva)) {
				if(anObject.tieneConflictos(tipoHabitacion,fechaInicio,fechaFin,EstadoReserva.NoTomada)) cantReser++;
			}
		}
		return cantReser;
	}
	
	public boolean confirmarDisponibilidad(Reserva reserva, TipoHabitacion tipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin) {
		int cantHab = this.calcCapacidad(tipoHabitacion);
		int canReser= this.calcReservaConDif(reserva, tipoHabitacion, fechaInicio, fechaFin);
		if(canReser<cantHab) {
			return true;
		}else {
			return false;
		}
	}
	
	public Set<Reserva> obtenerReservasPendientes(){
		HashSet<Reserva> returnreservas = new HashSet<Reserva>();
		for(Reserva r: reservas.values()) {
			if(r.getEstado()==EstadoReserva.Pendiente) {
				returnreservas.add(r);
			}
		}
		return returnreservas;
		
	}
	
	public Set<Reserva> obtenerReservas(){
		HashSet<Reserva> returnreservas = new HashSet<Reserva>();
		for(Reserva r: reservas.values()) {
			returnreservas.add(r);
		}
		return returnreservas;
		
	}
	public void agregarReserva(Reserva reserva) {
		this.reservas.put(reserva.getCodigo(), reserva);
	}
	
	public void quitarReserva(Reserva reserva) {
		this.reservas.remove(reserva.getCodigo());
	}
	public Reserva modificarReserva(Reserva reservaAntigua, Reserva reservaNueva) {
		this.reservas.remove(reservaAntigua.getCodigo());
		agregarReserva(reservaNueva);
		return reservaNueva;
	}
}
