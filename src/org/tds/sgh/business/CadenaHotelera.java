package org.tds.sgh.business;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

import org.tds.sgh.infrastructure.ICalendario;
import org.tds.sgh.infrastructure.Infrastructure;

import java.util.Set;

import javax.persistence.*;

@Entity
public class CadenaHotelera
{
	// --------------------------------------------------------------------------------------------
	
	protected long id;
	
	private Map<String, Cliente> clientes;
	
	private Map<String, Hotel> hoteles;
	
	private String nombre;
	
	private Map<String, TipoHabitacion> tiposHabitacion;
	
	private Cliente clienteSeleccionado;
	
	private Hotel hotelSeleccionado;
	
	
	// --------------------------------------------------------------------------------------------
	
	public CadenaHotelera(String nombre)
	{
		this.clientes = new HashMap<String, Cliente>();
		
		this.hoteles = new HashMap<String, Hotel>();
		
		this.nombre = nombre;
		
		this.tiposHabitacion = new HashMap<String, TipoHabitacion>();
		
		this.setClienteSeleccionado(null);
		
	}
	
	// --------------------------------------------------------------------------------------------
	
	public Cliente agregarCliente(
		String rut,
		String nombre,
		String direccion,
		String telefono,
		String mail) throws Exception{
		if (this.clientes.containsKey(rut)){
			throw new Exception("Ya existe un cliente con el RUT indicado.");
		}
		
		Cliente cliente = new Cliente(rut, nombre, direccion, telefono, mail);
		
		this.clientes.put(cliente.getRut(), cliente);
		
		return cliente;
	}
	
	public Hotel agregarHotel(String nombre, String pais) throws Exception{
		if (this.hoteles.containsKey(nombre)){
			throw new Exception("Ya existe un hotel con el nombre indicado.");
		}
		
		Hotel hotel = new Hotel(nombre, pais);
		
		this.hoteles.put(hotel.getNombre(), hotel);
		
		return hotel;
	}
	
	public TipoHabitacion agregarTipoHabitacion(String nombre) throws Exception
	{
		if (this.tiposHabitacion.containsKey(nombre))
		{
			throw new Exception("Ya existe un tipo de habitación con el nombre indicado.");
		}
		
		TipoHabitacion tipoHabitacion = new TipoHabitacion(nombre);
		
		this.tiposHabitacion.put(tipoHabitacion.getNombre(), tipoHabitacion);
		
		return tipoHabitacion;
	}
	
	public Cliente buscarCliente(String rut) throws Exception
	{
		Cliente cliente = this.clientes.get(rut);
		
		if (cliente == null)
		{
			throw new Exception("No existe un cliente con el nombre indicado.");
		}
		
		return cliente;
	}
	
	public Set<Cliente> buscarClientes(String patronNombreCliente)
	{
		if (patronNombreCliente == null){
			throw new NullPointerException("No existe un hotel con el nombre indicado.");
		}
		else{
		Set<Cliente> clientesEncontrados = new HashSet<Cliente>();
		
		for (Cliente cliente : this.clientes.values()){
			if (cliente.coincideElNombre(patronNombreCliente)){
				clientesEncontrados.add(cliente);
			}
		}
		
		return clientesEncontrados;
		}
	}
	
	public Hotel buscarHotel(String nombre) throws Exception
	{
		Hotel hotel = this.hoteles.get(nombre);
		
		if (hotel == null)
		{
			throw new Exception("No existe un hotel con el nombre indicado.");
		}
		
		return hotel;
	}
	
	public TipoHabitacion buscarTipoHabitacion(String nombre) throws Exception
	{
		TipoHabitacion tipoHabitacion = this.tiposHabitacion.get(nombre);
		
		if (tipoHabitacion == null)
		{
			throw new Exception("No existe un tipo de habitación con el nombre indicado.");
		}
		
		return tipoHabitacion;
	}
	//------------------------Setters y getters ----------------------//
	public String getNombre()
	{
		return this.nombre;
	}
	
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId()
	{
		return this.id;
	}
	
	protected void setId(long id)
	{
		this.id = id;
	}
	
	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="rut")
	public Map<String, Cliente> getClientes() {
		
		return this.clientes;
	}
	
	public void setClientes(Map<String, Cliente> clientes ) {
		
		this.clientes = clientes;
	}
	
	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="nombre")
	public Map<String, Hotel> getHotel() {
		
		return this.hoteles;
	}
	
	public void setHotel(Map<String, Hotel> hoteles ) {
		
		this.hoteles = hoteles;
	}
	
	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="id")
	public Map<String, TipoHabitacion> getTipoHabitacion() {
		
		return this.tiposHabitacion;
	}
	
	public void setTipoHabitacion(Map<String, TipoHabitacion> tiposHabitacion ) {
		
		this.tiposHabitacion = tiposHabitacion;
	}
	
	@OneToOne(cascade=CascadeType.ALL)
	public Cliente getClienteSeleccionado() {
		return this.clienteSeleccionado;
	}

	public void setClienteSeleccionado(Cliente clienteSeleccionado) {
		this.clienteSeleccionado = clienteSeleccionado;
	}
	
	
	public Set<Cliente> listarClientes() 
	{
		return new HashSet<Cliente>(this.clientes.values());
	}
	
	public Set<Hotel> listarHoteles()
	{
		return new HashSet<Hotel>(this.hoteles.values());
	}
	
	public Set<TipoHabitacion> listarTiposHabitacion()
	{
		return new HashSet<TipoHabitacion>(this.tiposHabitacion.values());
	}
	
	public boolean confirmarDiponibilidad(Reserva reserva, String nombreHotel, String nombreTipoHabitacion,GregorianCalendar fechaInicio, GregorianCalendar fechaFin ) throws Exception {
		TipoHabitacion tipoHabitacion = tiposHabitacion.get(nombreTipoHabitacion);
		ICalendario cal = Infrastructure.getInstance().getCalendario();
		if(cal.esAnterior(fechaInicio, cal.getHoy())) {
			throw new Exception("Sistema No Confirma Disponibilidad De fechas en el Pasado.");
		}
		if(cal.esPosterior(fechaInicio, fechaFin)) {
			throw new Exception("Fecha de Inicio Posterior a la Fecha de Fin.");
		}
		if(tipoHabitacion==null) {
			throw new Exception("Tipo Habitacion no existe.");
		}
		Hotel hotel = hoteles.get(nombreHotel);
		return hotel.confirmarDisponibilidad(reserva, tipoHabitacion, fechaInicio, fechaFin);
	}
	
	
	public Set<Hotel> sugerirAlternativas(String pais, String nombreTipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin) throws Exception{
		HashSet<Hotel> hoteles = new HashSet<Hotel>();
		TipoHabitacion th = this.tiposHabitacion.get(nombreTipoHabitacion);
		if(th==null) {
			throw new Exception("Tipo Habitacion no existe.");
		}
		boolean disp=false;
		for(Hotel h: this.hoteles.values()) {
			if(h.getPais()==pais) {
				disp = h.confirmarDisponibilidad(null, th, fechaInicio, fechaFin);
				if(disp) {
					hoteles.add(h);
				}
			}
		}
		return hoteles;
	}
	
	public boolean coincideNombreCliente(String nombre) {
		if(this.buscarClientes(nombre).size()>0) {
			return true;
		}else {
			return false;
		}
	}
	

	public Cliente registrarCliente(String rut, String nombre, String direccion, String telefono, String mail) throws Exception {
		Cliente cliente = new Cliente(rut, nombre, direccion, telefono, mail);
		if (this.clientes.containsKey(rut))
		{
			throw new Exception("No existe un cliente con el nombre indicado.");
		}
		this.clientes.put(cliente.getNombre(), cliente);
		return cliente;
	}
	
	public Reserva modificarReserva(Reserva reserva, Cliente cliente, String nombreHotel, String nombreTipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin, boolean modificablePorUsuario) throws Exception {
		TipoHabitacion th = this.buscarTipoHabitacion(nombreTipoHabitacion);
		Hotel h= this.buscarHotel(nombreHotel);
		//Reserva newReserva = cliente.modificarReserva(reserva, cliente.getRut(), h,th,fechaInicio,fechaFin,modificablePorUsuario);
		if(!reserva.isModificablePorHuesped()) {
			throw new Exception("Reserva no modficiable.");
		}
		return cliente.modificarReserva(reserva, cliente.getRut(), h,th,fechaInicio,fechaFin,modificablePorUsuario);
		
	}
	
	public Set<Reserva> buscarReservasPendientes(String nombreHotel) throws Exception{
		Hotel h = this.buscarHotel(nombreHotel);
		this.setHotelSeleccionado(h);
		return h.obtenerReservasPendientes();
	}
	
	public boolean registrarHuesped(Reserva reserva, Map<String, Huesped> huespedes) {
		for (Entry<String, Huesped> entry : huespedes.entrySet()) {
			reserva.registrarHuesped(entry.getValue().nombre, entry.getValue().documento);
		}
		return true;
	}

	

	@OneToOne(cascade=CascadeType.ALL)
	public Hotel getHotelSeleccionado() {
		return this.hotelSeleccionado;
	}
	
	public void setHotelSeleccionado(Hotel hotel) {
		this.hotelSeleccionado = hotel;
	}
}
