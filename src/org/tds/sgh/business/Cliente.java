package org.tds.sgh.business;
import javax.persistence.*;

import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.tds.sgh.infrastructure.ICalendario;
import org.tds.sgh.infrastructure.Infrastructure;
@Entity
public class Cliente
{
	// --------------------------------------------------------------------------------------------
	private long id;
	
	private String direccion;
	
	private String mail;
	
	private String nombre;
	
	private String rut;
	
	private String telefono;
	
	// --------------------------------------------------------------------------------------------
	private Map<Long, Reserva> reservas;
	private Reserva reserva;
	
	public Cliente(String rut, String nombre, String direccion, String telefono, String mail)
	{
		this.direccion = direccion;
		
		this.mail = mail;
		
		this.nombre = nombre;
		
		this.rut = rut;
		
		this.telefono = telefono;
		
		this.reservas = new HashMap<Long, Reserva>();
	}
	
	// --------------------------------------------------------------------------------------------
	
	public boolean coincideElNombre(String patronNombreCliente)
	{
		return this.nombre.matches(patronNombreCliente);
	}
	
	//---------------------------Getters y Setters-----------------------------------//
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public long getId()
	{
		return this.id;
	}
	
	protected void setId(long id)
	{
		this.id = id;
	}
	
	public String getDireccion()
	{
		return this.direccion;
	}
	
	public void setDireccion(String direccion)
	{
		this.direccion = direccion;
	}
	
	public String getMail()
	{
		return this.mail;
	}
	
	public void setMail(String mail)
	{
		this.mail = mail;
	}
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	
	public String getRut()
	{
		return this.rut;
	}
	
	public void setRut(String rut)
	{
		this.rut = rut;
	}
	
	public String getTelefono()
	{
		return this.telefono;
	}
	
	public void setTelefono(String telefono)
	{
		this.telefono = telefono;
	}
	
	@OneToMany(cascade=CascadeType.ALL)
	@MapKey(name="codigo")
	public Map<Long, Reserva> getReservas(){
		return this.reservas;
	}
	
	public void setReservas(Map<Long, Reserva> reservas){
		this.reservas = reservas;
	}
	
	@OneToOne(cascade=CascadeType.ALL)
	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}
	//--------------------------------------------------------------------
	
	
	public Reserva registrarReserva(String rut_cliente, Hotel hotel, TipoHabitacion tipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin, boolean modificablePorUsuario){
		Reserva reserva = new Reserva(rut_cliente, hotel, tipoHabitacion, fechaInicio, fechaFin, modificablePorUsuario, EstadoReserva.Pendiente);
		this.reservas.put(reserva.getCodigo(), reserva);
		return reserva;
	}
	
	
	public Reserva modificarReserva(Reserva reserva, String rutCliente, Hotel h, TipoHabitacion th, GregorianCalendar fechaInicio, GregorianCalendar fechaFin, boolean modificablePorUsuario) {
		Hotel hotel_antiguo =  reserva.getHotel();
		Reserva reservaAntigua = reserva;
		Reserva nueva = h.modificarReserva(reserva, reserva.modificarReserva(rutCliente,h,th,fechaInicio,fechaFin,modificablePorUsuario));
        hotel_antiguo.quitarReserva(reservaAntigua);;
		this.reserva = nueva;
        return this.reserva;
    }
	
	public Set<Reserva> buscarReservasDelCliente(){
		ICalendario cal = Infrastructure.getInstance().getCalendario();
		HashSet<Reserva> respuesta = new HashSet<Reserva>();
		String[] estados =  {EstadoReserva.NoTomada.toString(),EstadoReserva.Cancelada.toString(), EstadoReserva.Finalizada.toString(), EstadoReserva.Tomada.toString()};
		for(Reserva anObject : reservas.values()){
			//do someting to anObject...
			if(!Arrays.asList(estados).contains(anObject.getEstado().toString()) && !cal.esAnterior(anObject.getFechaInicio(), cal.getHoy())) {
				respuesta.add(anObject);
			}
		}
		return respuesta;
	}
	
	public Reserva seleccionarReserva(long codigoReserva) {
		return reservas.get(codigoReserva);
	}

	
	public void eliminarReserva(Reserva r) {
		this.reservas.remove(r.getCodigo());
		this.reservas.put(r.getCodigo(), r);
	}
	
	public void setReservasEmpty(){
		this.reservas.clear();
	}
}
