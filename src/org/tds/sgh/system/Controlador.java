package org.tds.sgh.system;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.tds.sgh.business.CadenaHotelera;
import org.tds.sgh.business.Cliente;
import org.tds.sgh.business.EstadoReserva;
import org.tds.sgh.business.Habitacion;
import org.tds.sgh.business.Hotel;
import org.tds.sgh.business.Reserva;
import org.tds.sgh.business.TipoHabitacion;
import org.tds.sgh.dtos.ClienteDTO;
import org.tds.sgh.dtos.DTO;
import org.tds.sgh.dtos.HabitacionDTO;
import org.tds.sgh.dtos.HotelDTO;
import org.tds.sgh.dtos.ReservaDTO;
import org.tds.sgh.dtos.TipoHabitacionDTO;
import org.tds.sgh.infrastructure.ICalendario;
import org.tds.sgh.infrastructure.Infrastructure;

public class Controlador implements IHacerReservaController, IModificarReservaController, ITomarReservaController, ICancelarReservaController, ICadenaController{	
	private Map<String, Hotel> maPhoteles;		
	private Map<String, TipoHabitacion> maPtiposHabitacion;		
	private Reserva reserva;
	private CadenaHotelera cadenahotelera;	
	private Cliente cliente;
	Infrastructure infrastructure = Infrastructure.getInstance();
	DTO dto = DTO.getInstance();
	
	public Controlador(CadenaHotelera cadenahotelera) {
		this.cadenahotelera = cadenahotelera;
		this.maPhoteles = new HashMap<String , Hotel>();
		this.maPtiposHabitacion = new HashMap<String,TipoHabitacion>();
		this.reserva=null;
		this.cliente=this.cadenahotelera.getClienteSeleccionado();
	}
	
	public Set<ClienteDTO> buscarClientes(String patronNombreCliente) throws Exception{		
		return dto.mapClientes(cadenahotelera.buscarClientes(patronNombreCliente));
	}
	
	public Set<ClienteDTO> buscarCliente(String patronNombreCliente) {
		return dto.mapClientes(cadenahotelera.buscarClientes(patronNombreCliente));
	}

	
	public ClienteDTO seleccionarCliente(String rut) throws Exception{
		this.cadenahotelera.setClienteSeleccionado(cadenahotelera.buscarCliente(rut));
		this.cliente=this.cadenahotelera.getClienteSeleccionado();
		return dto.map(this.cliente);		
	}
	
	public ClienteDTO registrarCliente(String rut, String nombre, String direccion, String telefono, String mail) throws Exception{
		this.cadenahotelera.setClienteSeleccionado(cadenahotelera.registrarCliente(rut, nombre, direccion, telefono, mail));
		this.cliente = this.cadenahotelera.getClienteSeleccionado();
		return dto.map(this.cliente);
	}
	
	public Set<ReservaDTO> buscarReservasDelCliente() throws Exception{		
		return dto.mapReserva(cliente.buscarReservasDelCliente());
	}
	
	public Set<ReservaDTO> buscarReservasPendientes(String nombreHotel) throws Exception{
		return dto.mapReserva(cadenahotelera.buscarReservasPendientes(nombreHotel));
	}
	
	public HotelDTO buscarHotel(String nombreHotel){		
		return dto.map(maPhoteles.get(nombreHotel));
	}
	
	public TipoHabitacionDTO buscarTipoHabitacion(String nombreTipoHabitacion){
		return dto.map(maPtiposHabitacion.get(nombreTipoHabitacion));		
	}
	
	public Set<HotelDTO> sugerirAlternativas(String pais, String nombreTipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin) throws Exception{
		ICalendario cal = Infrastructure.getInstance().getCalendario();
		
		if(cal.esAnterior(fechaInicio,cal.getHoy())){
			throw new Exception("Fecha de inicio menor a hoy.");
		}
		if(cal.esPosterior(fechaInicio, fechaFin)) {
			throw new Exception("Fecha de inicio es posterior a la fecha de fin.");
		}
		return dto.mapHoteles(cadenahotelera.sugerirAlternativas(pais, nombreTipoHabitacion, fechaInicio, fechaFin));
	}
		
	public ReservaDTO tomarReserva() throws Exception{
		this.reserva.cambiarEstado(EstadoReserva.Tomada);
		
		HashSet<Habitacion> habitacionesHotel = (HashSet<Habitacion>) this.reserva.getHotel().listarHabitaciones();
		boolean estaAsignada=false;
		for (Habitacion h : habitacionesHotel) {
			estaAsignada=false;
			for(Reserva anObject : this.reserva.getHotel().obtenerReservas()){
				if(anObject.getHabitacion()!=null) {
					if(anObject.getHabitacion().equals(h)) {
						estaAsignada=true;
					}					
				}								
			}
			if(!estaAsignada) {
				this.reserva.asignarHabitacion(h);
				break;
			}
		}		
		this.cliente=this.cadenahotelera.buscarCliente(this.reserva.getRutCliente());
		this.cadenahotelera.setClienteSeleccionado(this.cliente);
		this.enviarMail(this.cliente.getMail(), "Reserva realizada", "Se ha realizado una reserva, código " +this.reserva.getCodigo());
		ReservaDTO reservaDTO = dto.map(this.reserva);
		this.iniciarEstadia(reservaDTO);
		return dto.map(this.reserva);
		
	}
	
	public void enviarMail(String destinatario, String asunto, String mensaje){
		infrastructure.getSistemaMensajeria().enviarMail(destinatario, asunto, mensaje);
	}
	
	public void iniciarEstadia(ReservaDTO reserva){
		infrastructure.getSistemaFacturacion().iniciarEstadia(reserva);
	}
	
	public boolean confirmarDisponibilidad(String nombreHotel, String nombreTipoHabitacion, GregorianCalendar fechaInicio, GregorianCalendar fechaFin) throws Exception{
		return cadenahotelera.confirmarDiponibilidad(this.reserva, nombreHotel, nombreTipoHabitacion, fechaInicio, fechaFin);
	}
	
	public ReservaDTO seleccionarReserva(long codigoReserva) throws Exception {
		Set<Cliente> listaCliente;
		if(this.cliente!=null) {			
			this.reserva=this.cliente.seleccionarReserva(codigoReserva);
		}
		if(this.cliente==null && this.reserva == null && this.cadenahotelera.getHotelSeleccionado() == null) {			
			this.cadenahotelera.getClienteSeleccionado();
			throw new Exception();			
		}
		if (this.cliente == null){
			listaCliente = this.cadenahotelera.listarClientes();
			for (Cliente ite_cliente: listaCliente) {							
				this.reserva = ite_cliente.seleccionarReserva(codigoReserva);
				this.cliente = ite_cliente;
				this.cadenahotelera.setClienteSeleccionado(this.cliente);
			}
		}		
		return dto.map(this.reserva);
	}

	@Override
	public ReservaDTO cancelarReservaDelCliente() throws Exception {
		// TODO Auto-generated method stub
		this.reserva.cancelarReserva();
		this.cliente.eliminarReserva(this.reserva);
		this.enviarMail(cliente.getMail(), "Reserva realizada", "Se ha realizado una reserva, código");
		return dto.map(this.reserva);
	}

	@Override
	public ReservaDTO registrarHuesped(String nombre, String documento) throws Exception {
		// TODO Auto-generated method stub
		this.reserva.registrarHuesped(nombre,documento);
		return dto.map(this.reserva);
	}

	@Override
	public ReservaDTO modificarReserva(String nombreHotel, String nombreTipoHabitacion, GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin, boolean modificablePorHuesped) throws Exception {
		// TODO Auto-generated method stub
		this.cliente = this.cadenahotelera.getClienteSeleccionado();		
		this.reserva=cadenahotelera.modificarReserva(this.reserva, this.cliente, nombreHotel, nombreTipoHabitacion, fechaInicio, fechaFin, modificablePorHuesped);
		this.enviarMail(cliente.getMail(), "Reserva modificada", "Se ha realizado una reserva, código");
		return dto.map(this.reserva);
	}

	@Override
	public ReservaDTO registrarReserva(String nombreHotel, String nombreTipoHabitacion, GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin, boolean modificablePorHuesped) throws Exception {
		// TODO Auto-generated method stub
		Hotel h = this.cadenahotelera.buscarHotel(nombreHotel);
		TipoHabitacion th = this.cadenahotelera.buscarTipoHabitacion(nombreTipoHabitacion);	
		Reserva reserva = cliente.registrarReserva(cliente.getRut(), h, th, fechaInicio, fechaFin, modificablePorHuesped);
		h.agregarReserva(reserva);
		this.reserva=reserva;
		this.enviarMail(cliente.getMail(), "Reserva realizada", "Se ha realizado una reserva, código");
		this.cliente=null;
		this.cadenahotelera.setClienteSeleccionado(null);
		return dto.map(reserva);
	}

	@Override
	public ClienteDTO agregarCliente(String rut, String nombre, String direccion, String telefono, String mail) throws Exception {
		Cliente cliente = this.cadenahotelera.agregarCliente(rut, nombre, direccion, telefono, mail);
		this.cliente = cliente;
		return dto.map(cliente);
	}

	@Override
	public HabitacionDTO agregarHabitacion(String nombreHotel, String nombreTipoHabitacion, String nombre)
			throws Exception {
		// TODO Auto-generated method stub
		Hotel h = this.cadenahotelera.buscarHotel(nombreHotel);
		TipoHabitacion th = this.cadenahotelera.buscarTipoHabitacion(nombreTipoHabitacion);
		Habitacion habitacion = h.agregarHabitacion(th, nombre);
		return dto.map(h, habitacion);
	}

	@Override
	public HotelDTO agregarHotel(String nombre, String pais) throws Exception {
		// TODO Auto-generated method stub
		Hotel hotel = this.cadenahotelera.agregarHotel(nombre, pais);
		return dto.map(hotel);
	}

	@Override
	public TipoHabitacionDTO agregarTipoHabitacion(String nombre) throws Exception {
		// TODO Auto-generated method stub
		TipoHabitacion tipoHabitation = this.cadenahotelera.agregarTipoHabitacion(nombre);
		return dto.map(tipoHabitation);
	}

	@Override
	public Set<ClienteDTO> getClientes() {
		// TODO Auto-generated method stub
		HashSet<Cliente> clientes = (HashSet<Cliente>) this.cadenahotelera.listarClientes();
		return dto.mapClientes(clientes);
	}

	@Override
	public Set<HabitacionDTO> getHabitaciones(String nombreHotel) throws Exception {
		// TODO Auto-generated method stub
		Hotel hotel = this.cadenahotelera.buscarHotel(nombreHotel);
		HashSet<Habitacion> habitaciones = (HashSet<Habitacion>) hotel.listarHabitaciones();
		return dto.mapHabitaciones(hotel, habitaciones);
	}

	@Override
	public Set<HotelDTO> getHoteles() {
		// TODO Auto-generated method stub
		HashSet<Hotel> hoteles = (HashSet<Hotel>) this.cadenahotelera.listarHoteles();
		return dto.mapHoteles(hoteles);
	}

	@Override
	public Set<TipoHabitacionDTO> getTiposHabitacion() {
		// TODO Auto-generated method stub
		HashSet<TipoHabitacion> tiposhabitaciones = (HashSet<TipoHabitacion>) this.cadenahotelera.listarTiposHabitacion();
		return dto.mapTiposHabitacion(tiposhabitaciones);
	}
		
	
}